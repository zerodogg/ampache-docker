FROM php:7.4-apache-buster
LABEL maintainer="Eskild Hustvedt code@zerodogg.org"

# The ampache version
ARG version=5.6.1
# Checksum for the ampache zipfile
ARG zipfileChecksum=2b02fd2e9c2ed2566c9f1115153f851103f92154ff16c107dafb416bc166e38fe2266386b1c59ddc3e2ca96aea776953ba42d00e4a4474d7ea6ba8f20aa9e1a6

    # Fetch apt repo metadata
RUN apt-get update && \
    # Install base dependencies
    DEBIAN_FRONTEND=noninteractive apt-get -y install wget sudo gnupg2 unzip && \
    # Install libraries/codecs and tools that ampache needs
    DEBIAN_FRONTEND=noninteractive apt-get -y install inotify-tools lame libvorbisfile3 libvorbisenc2 vorbis-tools flac libmp3lame0 libavcodec-extra* libtheora0 libvpx5 ffmpeg git libpng-dev libjpeg-dev libfreetype6-dev libjpeg62-turbo libxml2 libxml2-dev && \
    # Install core PHP extensions (GD+MySQL)
    docker-php-ext-install pdo_mysql gd intl && \
    # Install secondary extensions needed for UPNP
    CFLAGS="-I/usr/src/php" docker-php-ext-install sockets xmlreader && \
    # Clean up
    apt-get clean && \
    DEBIAN_FRONTEND=noninteractive apt-get -y purge libpng-dev libjpeg-dev libfreetype6-dev libxml2-dev && \
    rm -rf /var/lib/apt/lists/*

    # Download, extract and install ampache
RUN wget --progress=bar:force:noscroll -O /opt/ampache.zip https://github.com/ampache/ampache/releases/download/$version/ampache-"$version"_all_php7.4.zip && \
    /usr/bin/test "`sha512sum /opt/ampache.zip|cut -d' ' -f 1`" = "$zipfileChecksum" && \
    rm -rf /var/www/html/* && \
    unzip -d /var/www/html/ /opt/ampache.zip && \
    # Fix ownership
    chown -R www-data /var/www/ && \
    # Remove git repo data that we don't need
    find /var/www/html/lib/vendor -name .git -type d -print0 |xargs -0 -- rm -rf && \
    # Move all the htaccess files into place
    for dir in $(find /var/www/html -name .htaccess.dist -print0|xargs -0 dirname); do cp $dir/.htaccess.dist $dir/.htaccess;done && \
    # Enable mod_rewrite
    ln -sf /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load && \
    # Modify the .dist config:
    # - comment out database_name and database_username by default
    # - set the default database_hostname to "mysql"
    perl -pi -E 's{^(database_username|database_name)}{;$1}g; s{^database_hostname\s*=.*}{database_hostname = mysql}' /var/www/html/config/ampache.cfg.php.dist && \
    # Use "public" as the directory for apache
    perl -pi -E 's{/var/www/html}{/var/www/html/public}g' /etc/apache2/sites-available/*.conf && \
    # Copy the .dist somewhere outside of the /config tree, so that we can
    # update it when needed
    cp /var/www/html/config/ampache.cfg.php.dist /ampache.cfg.php.dist && \
    # Clean up
    rm -f /opt/ampache.zip && \
    DEBIAN_FRONTEND=noninteractive apt-get -y purge --auto-remove g++-8 gcc-8 dpkg-dev libc-dev libgcc-8-dev libstdc++-8-dev linux-libc-dev zlib1g-dev libc-dev-bin

ADD run.sh /run.sh
RUN chmod a+x /run.sh

VOLUME ["/media"]
VOLUME ["/var/www/html/config"]
VOLUME ["/var/www/html/themes"]
EXPOSE 80

CMD ["/run.sh"]
