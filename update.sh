#!/bin/bash

VERSION=$1

if [ "$VERSION" == "" ]; then
    echo "USAGE: update.sh VERSION"
    exit 1
fi

ARCHIVE=ampache-${VERSION}_all_php7.4.zip

wget https://github.com/ampache/ampache/releases/download/$VERSION/$ARCHIVE || exit 1
CHECKSUM="$(sha512sum $ARCHIVE |cut -f 1 -d ' ')"

perl -pi -E "s{^ARG version=.+}{ARG version=$VERSION}; s{^ARG zipfileChecksum=.+}{ARG zipfileChecksum=$CHECKSUM}" Dockerfile

rm -f $ARCHIVE

echo "Updated Dockerfile to $VERSION with checksum $CHECKSUM"
